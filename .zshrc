#
# .zshrc is sourced in interactive shells.
# It should contain commands to set up aliases,
# functions, options, key bindings, etc.
#

autoload -U compinit
compinit

#allow tab completion in the middle of a word
setopt COMPLETE_IN_WORD

## keep background processes at full speed
#setopt NOBGNICE
## restart running processes on exit
#setopt HUP

## history
setopt INC_APPEND_HISTORY
# for sharing history between zsh processes
#setopt SHARE_HISTORY

## never ever beep ever
#setopt NO_BEEP

## automatically decide when to page a list of completions
#LISTMAX=0

## disable mail checking
#MAILCHECK=0

# autoload -U colors
#colors

PATH=$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting

[ -s $HOME/.nvm/nvm.sh ] && . $HOME/.nvm/nvm.sh # This loads NVM

#export PATH="/home/project-web/wvm/python/bin:$PATH"

export PATH="/home/users/w/wp/wpshell/bin:$PATH"

export PATH="/home/users/w/wp/wpshell/.nvm/v0.11.10/bin:$PATH"
